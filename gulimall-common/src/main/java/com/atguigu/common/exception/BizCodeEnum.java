package com.atguigu.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 错误码和错误信息定义类 <br/>
 * * 1. 错误码定义规则为5位数字 <br/>
 * * 2. 前两位表示业务场景，最后三位表示错误码。例如：100001。10:通用 001:系统未知异常 <br/>
 * * 3. 维护错误码后需要维护错误描述，将他们定义为枚举形式 <br/>
 * * 错误码列表：<br/>
 * *  10: 通用 <br/>
 * *      001：参数格式校验 <br/>
 * *  11: 商品 <br/>
 * *  12: 订单 <br/>
 * *  13: 购物车 <br/>
 * *  14: 物流 <br/>
 *
 * @author Don
 */
@AllArgsConstructor
@Getter
public enum BizCodeEnum {
    /**
     * 系统未知异常
     */
    UNKNOWN_EXCEPTION(10000, "系统未知异常"),
    /**
     * 参数格式校验失败
     */
    VALID_EXCEPTION(10001, "参数格式校验失败"),
    TOO_MANY_REQUEST(10002, "请求流量过大"),
    SMS_CODE_EXCEPTION(10002, "验证码获取频率太高，稍后再试"),
    PRODUCT_UP_EXCEPTION(11000, "商品上架异常"),
    USER_EXIST_EXCEPTION(15001, "用户存在"),
    PHONE_EXIST_EXCEPTION(15002, "手机号存在"),
    NO_STOCK_EXCEPTION(21000, "商品库存不足"),
    LOGINACCT_PASSWORD_INVAILD_EXCEPTION(15003, "账号密码错误");
    private final int code;
    private final String msg;
}
