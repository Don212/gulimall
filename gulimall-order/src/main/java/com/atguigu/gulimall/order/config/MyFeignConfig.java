package com.atguigu.gulimall.order.config;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Configuration
public class MyFeignConfig {
    /**
     * Feign 远程调用丢失请求头问题
     * <p>
     * 加上feign远程调用的请求拦截器
     */
    @Bean("requestInterceptor")
    public RequestInterceptor requestInterceptor() {
        return template -> {
            //1、RequestContextHolder拿到刚进来的这个请求
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if (attributes != null) {
                System.out.println("RequestInterceptor线程...." + Thread.currentThread().getId());
                HttpServletRequest request = attributes.getRequest(); //老请求
                //同步请求头数据 Cookie
                String cookie = request.getHeader("Cookie");
                //给新请求同步了老请求的cookie
                template.header("Cookie", cookie);
            }
        };
    }
}