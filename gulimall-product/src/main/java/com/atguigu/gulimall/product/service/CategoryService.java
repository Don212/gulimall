package com.atguigu.gulimall.product.service;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.vo.Catelog2Vo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author Don
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 树形结构列表
     *
     * @return {@link List<CategoryEntity>}
     */
    List<CategoryEntity> listWithTree();

    /**
     * 删除菜单 ids
     *
     * @param catIds id列表
     * @return int
     */
    int removeMenuByIds(List<Long> catIds);

    /**
     * 找到catelogId的完整路径 [父/子/孙]
     */
    Long[] findCatelogPath(Long catelogId);

    void updateCascade(CategoryEntity category);

    List<CategoryEntity> getLevel1Categorys();

    Map<String, List<Catelog2Vo>> getCatalogJson();
}
