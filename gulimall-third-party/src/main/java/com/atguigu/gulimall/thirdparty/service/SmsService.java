package com.atguigu.gulimall.thirdparty.service;

public interface SmsService {
    void sendSmsCode(String phone, String code) throws Exception;
}
