package com.atguigu.gulimall.cart.vo;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * 购物项内容
 */

@Setter
public class CartItem {
    @Getter
    private Long skuId;
    @Getter
    private Boolean check = true;
    @Getter
    private String title;
    @Getter
    private String image;
    @Getter
    private List<String> skuAttr;
    @Getter
    private BigDecimal price;
    @Getter
    private Integer count;
    private BigDecimal totalPrice;

    /**
     * 计算当前项的总价
     */
    public BigDecimal getTotalPrice() {
        return this.price.multiply(new BigDecimal("" + this.count));
    }
}
