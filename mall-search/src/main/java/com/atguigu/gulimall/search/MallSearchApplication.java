package com.atguigu.gulimall.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Elasticsearch 检索服务
 *
 * @author Don
 */
@EnableFeignClients(basePackages = {"com.atguigu.gulimall.search.feign"})
@EnableDiscoveryClient
@SpringBootApplication
public class MallSearchApplication {
    public static void main(String[] args) {
        SpringApplication.run(MallSearchApplication.class, args);
    }
}