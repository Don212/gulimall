package com.atguigu.gulimall.search.constant;

public class EsConstant {
    public static final String PRODUCT_INDEX = "gulimall_product2"; //sku数据在es中的索引
    public static final Integer PRODUCT_PAGE_SIZE = 10; //ES查询分页大小
}
